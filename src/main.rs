use rocket::{get, routes};

mod challenge;
mod markdown;

use markdown::{Markdown, MarkdownConvertError};

type MarkdownResult = Result<Markdown, MarkdownConvertError>;

#[get("/")]
fn index() -> MarkdownResult {
    one()
}

#[get("/1")]
fn one() -> MarkdownResult {
    include_str!("../static/page1.md").parse()
}

#[get("/2")]
fn two() -> MarkdownResult {
    include_str!("../static/page2.md").parse()
}

#[shuttle_runtime::main]
async fn main() -> shuttle_rocket::ShuttleRocket {
    #[cfg(debug_assertions)]
    dbg!(challenge::lists::all());
    Ok(rocket::build().mount("/", routes![index, one, two]).into())
}
