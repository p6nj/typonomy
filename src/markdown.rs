use std::str::FromStr;

use amplify::{From, Wrapper};
use md::{message::Message, to_html_with_options, CompileOptions, Options};
use rocket::{
    http::Status,
    response::{content::RawHtml, status, Responder},
};

#[derive(Responder)]
pub(super) struct Markdown(RawHtml<String>);

#[derive(Wrapper, From)]
pub(super) struct MarkdownConvertError(Message);

impl<'r> Responder<'r, 'static> for MarkdownConvertError {
    fn respond_to(self, request: &'r rocket::Request<'_>) -> rocket::response::Result<'static> {
        status::Custom(Status::InternalServerError, self.0.to_string()).respond_to(request)
    }
}

impl FromStr for Markdown {
    type Err = MarkdownConvertError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        to_html_with_options(
            s,
            &Options {
                compile: CompileOptions {
                    allow_dangerous_html: true,
                    allow_dangerous_protocol: true,
                    ..Default::default()
                },
                ..Default::default()
            },
        )
        .map(RawHtml)
        .map(Markdown)
        .map_err(Into::into)
    }
}
