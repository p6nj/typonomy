use std::fmt::Debug;

use getset2::Getters;
use quote::ToTokens;
use syn::Expr;

pub(super) mod lists;

#[derive(Getters)]
pub(super) struct Challenge {
    #[getset(get = "pub(super)")]
    code: Expr,
    #[getset(get = "pub(super)")]
    type_name: &'static str,
}

impl Challenge {
    pub(super) fn code_str(&self) -> syn::Result<String> {
        Self::format_code(self.code.to_token_stream().to_string())
    }

    pub(super) fn into_code_str(self) -> syn::Result<String> {
        Self::format_code(self.code.into_token_stream().to_string())
    }

    fn format_code(code: impl AsRef<str>) -> syn::Result<String> {
        Ok(prettyplease::unparse(&syn::parse_file(
            &["fn main()", code.as_ref()].concat(),
        )?))
    }
}

#[cfg(debug_assertions)]
impl Debug for Challenge {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("Challenge")
            .field("code", &self.code().to_token_stream().to_string())
            .field("type_name", self.type_name())
            .finish()
    }
}
