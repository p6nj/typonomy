use std::collections::HashMap;

use super::Challenge;

macro_rules! challenges {
    ( $( $key:expr => $expr:expr ),* $(,)? ) => {{
        HashMap::from([
            $(
                (
                    $key,
                    Challenge {
                        code: {
                            let expr: syn::Expr = syn::parse_quote!($expr);
                            expr
                        },
                        type_name: std::any::type_name_of_val(&($expr)),
                    }
                ),
            )*
        ])
    }};
}

pub fn all() -> HashMap<u16, Challenge> {
    challenges! {
        1 => {
            let a: u8 = 3;
            let b: u8 = 4;
            a + b
        }
    }
}
