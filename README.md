# Typonomy :D

Quizz time! Typonomy is a Rust type guessing game.

Visit the [official website](https://typonomy-eoe6.shuttle.app/) or run it locally with [`shuttle`](https://docs.shuttle.dev/getting-started/installation) :

```sh
shuttle run --release
```
